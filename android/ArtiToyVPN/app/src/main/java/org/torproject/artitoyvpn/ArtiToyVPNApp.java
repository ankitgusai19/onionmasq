package org.torproject.artitoyvpn;

import android.app.Application;

import org.torproject.artitoyvpn.ui.logging.LogObservable;
import org.torproject.artitoyvpn.vpn.VpnStatusObservable;

public class ArtiToyVPNApp extends Application {

    VpnStatusObservable vpnStatusObservable;
    LogObservable logObservable;
    @Override
    public void onCreate() {
        super.onCreate();
        vpnStatusObservable = VpnStatusObservable.getInstance();
        logObservable = LogObservable.getInstance();
    }
}
