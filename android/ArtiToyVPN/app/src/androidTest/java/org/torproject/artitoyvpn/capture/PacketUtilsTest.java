package org.torproject.artitoyvpn.capture;

import android.content.Context;

import androidx.test.espresso.internal.inject.InstrumentationContext;
import androidx.test.platform.app.InstrumentationRegistry;

import junit.framework.TestCase;

public class PacketUtilsTest extends TestCase {

    public void testConvertHexToIP() {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        PacketUtils packetUtils = new PacketUtils(context);
        assertEquals("127.0.0.1", packetUtils.convertHexToIP("0100007F"));
        assertEquals("192.168.1.38", packetUtils.convertHexToIP("2601A8C0"));
    }
}