use smoltcp::{
    socket::{TcpSocket, TcpSocketBuffer},
    wire::{IpProtocol, Ipv4Packet, TcpPacket},
};

use crate::device::Packet;

pub struct Parser {}

impl Parser {
    fn parse_tcp_packet(packet: &TcpPacket<&[u8]>) -> Option<TcpSocket<'static>> {
        if packet.syn() && !packet.ack() {
            Some(TcpSocket::new(
                TcpSocketBuffer::new(vec![0; 4096]),
                TcpSocketBuffer::new(vec![0; 4096]),
            ))
        } else {
            None
        }
    }

    fn parse_ipv4_packet(packet: &Ipv4Packet<&[u8]>) -> Option<TcpSocket<'static>> {
        match packet.protocol() {
            IpProtocol::Tcp => match TcpPacket::new_checked(packet.payload()) {
                Ok(tcp_packet) => {
                    let mut tcp_socket = Parser::parse_tcp_packet(&tcp_packet);
                    if let Some(ref mut socket) = tcp_socket {
                        let src_addr = packet.src_addr();
                        let src_port = tcp_packet.src_port();
                        let dst_addr = packet.dst_addr();
                        let dst_port = tcp_packet.dst_port();

                        log::info!(
                            "New incoming TCP connection: {}:{} -> {}:{}",
                            src_addr,
                            src_port,
                            dst_addr,
                            dst_port
                        );

                        assert!(!socket.is_open());
                        assert!(!socket.is_active());

                        socket.listen((dst_addr, dst_port)).unwrap();
                    }
                    return tcp_socket;
                }
                Err(err) => {
                    log::error!("Unable to create TCP packet: {}", err);
                }
            },
            _ => {} // Ignore non TCP streams.
        }
        None
    }

    pub fn parse(packet: Packet) -> Option<TcpSocket<'static>> {
        if let Ok(packet) = Ipv4Packet::new_checked(packet.as_slice()) {
            Parser::parse_ipv4_packet(&packet)
        } else {
            None
        }
    }
}
